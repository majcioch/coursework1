# Makefile for Writing Make Files Example
CC = g++
CXXFLAGS= -g -Wall -Wextra -Wpedantic

theatre_client: theatre_client.cpp events.o film.o music.o comedy.o booking.o
	$(CC) $(CXXFLAGS) -o $@ $^
events.o: events.cpp events.h 
	$(CC) $(CXXFLAGS) -c $<
film.o: film.cpp film.h 
	$(CC) $(CXXFLAGS) -c $<
music.o: music.cpp music.h 
	$(CC) $(CXXFLAGS) -c $<
comedy.o: comedy.cpp comedy.h 
	$(CC) $(CXXFLAGS) -c $<
booking.o: booking.cpp booking.h 
	$(CC) $(CXXFLAGS) -c $<

.PHONY : clean

clean:
	rm *.o
	rm *.exe
	rm *.txt
