/*music.cpp
Author: M00770353<KU066@live.mdx.ac.uk>
Created: 14/01/2022
Updated: 14/01/2020
*/
#include "music.h"

Music::Music()
{
	taken_seats = 0;
	for (int i = 0; i < music_seats_available; i++)
	{
		all_booked[i] = Booking("", "", i + 1, false);
	}
}

Music::Music(int my_taken_seats)
{
	taken_seats = my_taken_seats;
	for (int i = 0; i < music_seats_available; i++)
	{
		all_booked[i] = Booking("", "", i + 1, false);
	}
}

Music::~Music() {}

void Music::set_taken_seats(int my_taken_seats)
//setup booking if we read from file
{
	//booking from file
	taken_seats = my_taken_seats;
	taken_seats = (music_seats_available - taken_seats);
	for (int i = 0; i < taken_seats; i++)
	{
		all_booked[i].add_booking_not_allocated();
    //adding booking for the value of taken seats that were in file
	}
}

int Music::get_all_free_seats()
//return how many free seats are left
{
	int free = 0;
	int i = 0;
	for (i = i; i < music_seats_available; i++)
	{
		if (!all_booked[i].is_reserved())
		{
			free++;
		}
	}
	return free;
}

void Music::add_booking()
//adding booking by passing first free seat to object of booking
{
	std::cout << "Adding booking" << std::endl;
	std::cout << "-------------------------------" << std::endl;
	int chosen_id = 0;
	int i = 0;
	if (get_all_free_seats() != 0)
	{
		while (chosen_id == 0)
		{
			if (!all_booked[i].is_reserved())
			{
				chosen_id = i;
				all_booked[chosen_id].add_booking_not_allocated();
				std::cout << "Standing ticket for : " <<
					get_event_name() << std::endl;
				chosen_id++;	//for loop to break

			}

			i++;
		};
	}
	else
	{
		std::cout << "No free seats avible." << std::endl;
	}
}

void Music::remove_booking()
//removes highest index of booking
{
	std::cout << "Remove booking" << std::endl;
	int temp = 0;
	temp = 200 - get_all_free_seats();	//get number of booked seats
	all_booked[temp - 1].remove_booking();	//remove booking with index of temp-1(becouse counting from 0)
}