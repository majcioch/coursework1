/* theatre_client.cpp
Author: M00770353 <KU066@live.mdx.ac.uk>
Created: 06/01/2022
Updated: 14/01/2020
*/

#include "booking.h"
#include "events.h"
#include "music.h"
#include "comedy.h"
#include "film.h"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>

const int film_ArrSize = 3;
// number of movies
const int comedy_ArrSize = 2;
// number of stand-ups comedy
const int music_ArrSize = 2;
// number of music events
std::vector<Film> ftTab;
std::vector<Comedy> ctTab;
std::vector<Music> mtTab;
void pause2()	//pausing(waiting for user input)
{
	std::cin.clear();
	std::cin.ignore();
	std::string dummy;
	std::cout << "Press any key to continue . . .";
	std::getline(std::cin, dummy);
}

void save_film() //writing event info from vectors of objects to file 
{
	std::ofstream file("FilmDataDump.txt");
	for (Film b: ftTab)
	{
		file << b.get_event_name() << "; " << b.get_event_date() <<
			"; " << b.get_ticket_price() << "; " << b.get_film_type() << "; " <<
			b.get_all_free_seats() << "; ";
	}

	std::cout << "Film-Data saved" << std::endl;

}

void save_comedy()	//writing event info from vectors of objects to file 
{
	std::ofstream file("ComedyDataDump.txt");

	for (Comedy b: ctTab)
	{
		file << b.get_event_name() << "; " << b.get_event_date() <<
			"; " << b.get_ticket_price() << "; " <<
			b.get_all_free_seats() << ";";
	}

	std::cout << "Comedy-Data saved" << std::endl;

}

void save_music() //writing event info from vectors of objects to file 
{
	std::ofstream file("MusicDataDump.txt");

	for (Music b: mtTab)
	{
		file << b.get_event_name() << "; " << b.get_event_date() <<
			"; " << b.get_ticket_price() << "; " <<
			b.get_all_free_seats() << "; ";
	}
	std::cout << "Music-Data saved" << std::endl;

}

void read_film() //film file
{
	//reading from a file and overwriting data to classes
	int count = 0;
	int i = 0;
	std::ifstream filein("FilmDataDump.txt");
	std::string x = "";
	std::cout << "Reading file:" << std::endl;
	if (filein.is_open())
	{
		while (!filein.eof()) //if not end of file
		{
			std::getline(filein, x, ';');//';' is data separator
			if (count >= (film_ArrSize))
			{
				std::cout << x << "reading complete" << std::endl;
			}
			else
			{
				if (i == 0)
				{
					std::cout << std::endl << "Event name:" << std::endl;
					ftTab[count].set_event_name(x); //setup event from file
					i++;
				}
				else if (i == 1)
				{
					std::cout << "Event date:" << std::endl;
					ftTab[count].set_event_date(x);//setup event from file
					i++;
				}
				else if (i == 2)
				{
					std::cout << "Ticket price:" << std::endl;
					ftTab[count].set_ticket_price(std::stol(x));
					//setup event from file
					//change string to int
					i++;
				}
				else if (i == 3)
				{
					std::cout << "Movie type:" << std::endl;
					//setup event from file
					ftTab[count].set_film_type(x);
					i++;
				}
				else if (i == 4)
				{
					std::cout << "Booking and free seats:" <<
						std::endl;
					ftTab[count].set_taken_seats(std::stol(x));
					//setup event from file
					count++;
					i = 0;
				}
			}

			std::cout << x << std::endl;

		}
	}
	else std::cout << "Unable to read file" << std::endl;
}

void read_comedy()//comedy file
{
	//reading from a file and overwriting fata to classes
	int count = 0;
	int i = 0;
	std::ifstream filein("ComedyDataDump.txt");
	std::string x = "";
	std::cout << "Reading file:" << std::endl;
	if (filein.is_open())
	{
		while (!filein.eof())//while not end of file
		{
			std::getline(filein, x, ';');//';' is data separator
			if (count >= (comedy_ArrSize))
			{
				std::cout << x << "reading complete" << std::endl;
			}
			else
			{
				if (i == 0)
				{
					std::cout << std::endl << "Event name:" << std::endl;
					ctTab[count].set_event_name(x);//setup event from file
					i++;
				}
				else if (i == 1)
				{
					std::cout << "Event date:" << std::endl;
					ctTab[count].set_event_date(x);//setup event from file
					i++;
				}
				else if (i == 2)
				{
					std::cout << "Ticket price:" <<
						std::endl;
					ctTab[count].set_ticket_price(std::stol(x));
					//setup event from file / change string to int
					i++;
				}
				else if (i == 3)
				{
					std::cout << "Booking and free seats:" <<
						std::endl;
					ctTab[count].set_taken_seats(std::stol(x));
					//setup event from file / change string to int
					count++;
					i = 0;
				}
			}

			std::cout << x << std::endl;

		}
	}
	else std::cout << "Unable to read file" << std::endl;
}

void read_music()//music
{
	//reading from a file and overwriting fata to classes
	int count = 0;
	int i = 0;
	std::ifstream filein("MusicDataDump.txt");
	std::string x = "";
	std::cout << "Reading file:" << std::endl;
	if (filein.is_open())
	{
		while (!filein.eof())//while not end of file
		{
			std::getline(filein, x, ';');//';' is data separator
			if (count >= (music_ArrSize))
			{
				std::cout << x << "reading complete" << std::endl;
			}
			else
			{
				if (i == 0)
				{
					std::cout << std::endl << "Event name:" << std::endl;
					mtTab[count].set_event_name(x);
					//setup event from file
					i++;
				}
				else if (i == 1)
				{
					std::cout << "Event date:" << std::endl;
					mtTab[count].set_event_date(x);
					//setup event from file
					i++;
				}
				else if (i == 2)
				{
					std::cout << "Ticket price:" << std::endl;
					mtTab[count].set_ticket_price(std::stol(x));
					//setup event from file / change string to int
					i++;
				}
				else if (i == 3)
				{
					std::cout << "Booking and free seats::" <<
						std::endl;
					mtTab[count].set_taken_seats(std::stol(x));
					//setup event from file / change string to int
					count++;
					i = 0;
				}
			}

			std::cout << x << std::endl;

		}
	}
	else std::cout << "Unable to read file" << std::endl;
}

void menu()//cout menu for theatre staff
{
	std::cout << "Theatre" << std::endl;
	std::cout << "------------" << std::endl;
	std::cout << "Choose functionality:" << std::endl;
	std::cout << "1 - Add a booking for an event" << std::endl;
	std::cout << "2 - Cancel/Refund a booking" << std::endl;
	std::cout << "3 - List all events" << std::endl;
	std::cout << "4 - List details and availability of a given event" <<
		std::endl;
	std::cout << "5 - Load data from a file (all events and availability)" <<
		std::endl;
	std::cout << "6 - Save data to file (all events and availability)" <<
		std::endl;
	std::cout << "0 - Close" << std::endl;
}

int choose()//cout event types and return users input
{
	int x;
	std::cout << "Choose event" << std::endl << std::endl;
	std::cout << "------------" << std::endl;
	std::cout << "1 - Live music" << std::endl;
	std::cout << "2 - Stand-up comedy" << std::endl;
	std::cout << "3 - Film" << std::endl;
	std::cin >> x;
	return x;
}

void setupComedy(Comedy &a, int x)//setting up obejcts for comedy
{
	std::string comedy_titles[comedy_ArrSize] = { "TREVOR NOAH",
		"Kevin Hart " };
	double comedy_ticket_price[comedy_ArrSize] = { 100, 75 };
	std::string date[comedy_ArrSize] = { "12.02.2022", "23.01.2022" };

	a.set_event_name(comedy_titles[x]);
	a.set_ticket_price(comedy_ticket_price[x]);
	a.set_event_date(date[x]);
	a.set_event_id(x + 1);
}

void setupFilm(Film &a, int x)//setting up obejcts for film
{
	std::string film_titles[film_ArrSize] = { "Lord of the rings",
		"Avatar",
		"Diuna" };
	std::string film_types[film_ArrSize] = { "2D",
		"3D",
		"3D" };
	double film_ticket_price[film_ArrSize] = { 12.5, 13, 13 };
	std::string date[film_ArrSize] = { "12.01.2022", "14.01.2022", "16.01.2022" };

	a.set_event_name(film_titles[x]);
	a.set_film_type(film_types[x]);
	a.set_ticket_price(film_ticket_price[x]);
	a.set_event_date(date[x]);
	a.set_event_id(x + 1);

}

void setupMusic(Music &a, int x)//setting up obejcts for music
{
	std::string music_titles[music_ArrSize] = { "Justin Bieber Live Music Event 2022!",
		"Ariana Grande !Live Show! " };
	double music_ticket_price[music_ArrSize] = { 55, 69 };
	std::string date[music_ArrSize] = { "13.01.2022", "09.06.2022" };

	a.set_event_name(music_titles[x]);
	a.set_ticket_price(music_ticket_price[x]);
	a.set_event_date(date[x]);
	a.set_event_id(x + 1);
}

void c_print(std::vector<Comedy> a)//printing all info from comedy
{
	std::cout << "↓↓  All Stand-up comedies event: ↓↓" << std::endl;
	for (int i = 0; i < comedy_ArrSize; i++)
	{
		a[i].display_all();
	}

	std::cout << std::endl;
}

void m_print(std::vector<Music> a)//printing all info from music
{
	std::cout << "↓↓  All music events:  ↓↓" << std::endl;
	for (int i = 0; i < music_ArrSize; i++)
	{
		a[i].display_all();
	}

	std::cout << std::endl;
}

void f_print(std::vector<Film> a)//printing all info from film
{
	std::cout << "↓↓  All movies playing:  ↓↓" << std::endl;
	for (int i = 0; i < film_ArrSize; i++)
	{
		a[i].display_all();
		a[i].get_film_type();
	}

	std::cout << std::endl;
}

int main()
{
	Comedy ct[comedy_ArrSize];
	Film ft[film_ArrSize];
	Music mt[music_ArrSize];

	for (int i = 0; i < comedy_ArrSize; i++)	//loop to setup comedies events
	{
		setupComedy(ct[i], i);
		ctTab.push_back(ct[i]);
	}

	for (int i = 0; i < film_ArrSize; i++)	//loop to setup film events
	{
		setupFilm(ft[i], i);
		ftTab.push_back(ft[i]);
	}

	for (int i = 0; i < music_ArrSize; i++)	//loop to setup comedies events
	{
		setupMusic(mt[i], i);
		mtTab.push_back(mt[i]);
	}

	menu();
	int choice;
	std::size_t temp;	// cant be int becouse later is commpared unsigned int
	std::cin >> choice;
	while (choice > 0 && choice < 7)
	{
		switch (choice)
		{
			case 1: //booking
				{
					temp = choose();//choosing event type
					switch (temp)
					{
						case 1:
							m_print(mtTab);//printing all obejcts in class music
							std::cout << std::endl << "Choose event by id" << std::endl;
							std::cin >> temp;
							if (temp > mtTab.size())
							{
								std::cout << "wrong input" << std::endl;
								break;
							}

							mtTab[temp - 1].add_booking();
							break;
						case 2:
							c_print(ctTab);//printing all obejcts in class comedy
							std::cout << std::endl << "Choose event by id" << std::endl;
							std::cin >> temp;
							if (temp > ctTab.size())
							{
								std::cout << "wrong input" << std::endl;
								break;
							}

							ctTab[temp - 1].add_booking();
							break;
						case 3:
							f_print(ftTab);//printing all obejcts in class film
							std::cout << std::endl << "Choose event by id" << std::endl;
							std::cin >> temp;
							if (temp > ftTab.size())
							{
								std::cout << "wrong input" << std::endl;
								break;
							}

							ftTab[temp - 1].add_booking();
							break;

					}

					pause2();
					break;
				}

			case 2://remove booking
				{
					temp = choose();
					switch (temp)
					{
						case 1:
							m_print(mtTab);
							std::cout << std::endl << "Choose event by id" << std::endl;
							std::cin >> temp;
							if (temp > mtTab.size())
							{
								std::cout << "wrong input" << std::endl;
								break;
							}

							mtTab[temp - 1].remove_booking();
							break;
						case 2:
							c_print(ctTab);
							std::cout << std::endl << "Choose event by id" << std::endl;
							std::cin >> temp;
							if (temp > ctTab.size())
							{
								std::cout << "wrong input" << std::endl;
								break;
							}

							ctTab[temp - 1].remove_booking();
							break;
						case 3:
							f_print(ftTab);
							std::cout << std::endl << "Choose event by id" << std::endl;
							std::cin >> temp;
							if (temp > ftTab.size())
							{
								std::cout << "wrong input" << std::endl;
								break;
							}

							ftTab[temp - 1].remove_booking();
							break;

					}

					pause2();
					break;
				}

			case 3: //list all events
				{
					m_print(mtTab);
					std::cout << "*******************************" << std::endl;
					std::cout << std::endl;
					c_print(ctTab);
					std::cout << "*******************************" << std::endl;
					std::cout << std::endl;
					f_print(ftTab);
					pause2();
					break;
				}

			case 4://event details
				{
					temp = choose();
					switch (temp)
					{
						case 1:
							m_print(mtTab);
							std::cout << std::endl << "Choose event by id" << std::endl;
							std::cin >> temp;
							if (temp > mtTab.size())
							{
								std::cout << "wrong input" << std::endl;
								break;
							}

							mtTab[temp - 1].display_all();
							std::cout<<"Free seats: "<<mtTab[temp - 1].get_all_free_seats()<<std::endl;
							std::cout << std::endl;
							break;
						case 2:
							c_print(ctTab);
							std::cout << std::endl << "Choose event by id" << std::endl;
							std::cin >> temp;
							if (temp > ctTab.size())
							{
								std::cout << "wrong input" << std::endl;
								break;
							}

							ctTab[temp - 1].get_all_free_seats();
							ctTab[temp - 1].display_all();

							std::cout << std::endl;
							break;
						case 3:
							f_print(ftTab);
							std::cout << std::endl << "Choose event by id" << std::endl;
							std::cin >> temp;
							if (temp > ftTab.size())
							{
								std::cout << "wrong input" << std::endl;
								break;
							}

							ftTab[temp - 1].display_all();
							std::cout << "Movie type : " << ftTab[temp - 1].get_film_type();
							std::cout<<"Free seats: "<<ftTab[temp - 1].get_all_free_seats()<<std::endl;

							std::cout << std::endl;
							break;

					}

					pause2();
					break;
				}

			case 5://read from files
				{
					read_film();
					read_comedy();
					read_music();
					pause2();
					break;
				}

			case 6://write to files
				save_film();
				save_comedy();
				save_music();
				pause2();
				break;
		}

		menu();//print menu
		std::cin >> choice;
	}
}