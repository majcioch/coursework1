/*comedy.cpp
Author: M00770353<KU066@live.mdx.ac.uk>
Created: 14/01/2022
Updated: 14/01/2020
*/
#include "comedy.h"
#include <vector>

Comedy::Comedy()
{
	for (int i = 0; i < comedy_seats_available; i++)
	{
		all_booked[i] = Booking("", "", i + 1, false);
	}
}

Comedy::~Comedy() {}

void Comedy::set_taken_seats(int my_taken_seats)
//for reading how many seats were taken from file
{
	taken_seats = my_taken_seats;
	taken_seats = (comedy_seats_available - taken_seats);
	for (int i = 0; i < taken_seats; i++)
	{
		all_booked[i].add_booking_not_allocated();
		//adding booking for bookings from file
	}
}

int Comedy::get_all_free_seats()
//printing and returning how many seats are free
{
	int free = 0;
	int i = 0;
	for (i = i; i < comedy_seats_available; i++)
	{
		all_booked[i].print();
		if (!all_booked[i].is_reserved())
		{
			free++;
		}

		if (i % 4 == 3)	//for better visuals
			std::cout << std::endl;
	}

	std::cout << std::endl << "Free seats: " << free << std::endl;
	return free;
}

void Comedy::add_booking()
//adding booking for comedy with users chosen seat
{
	std::cout << "Add booking:" << std::endl;
	std::cout << "------------------------" << std::endl;
	get_all_free_seats();
	std::cout << "Select free seat" << std::endl;
	int chosen_seat;
	std::cin >> chosen_seat;
	if (chosen_seat > comedy_seats_available)
	{
		std::cout << "Wrong seat number, try again" << std::endl;
		add_booking();
	}
	else if (chosen_seat > 0)
	{
		all_booked[chosen_seat - 1].add_booking();
	}
}

void Comedy::remove_booking()
//removing booking for comedy with users chosen seat
{
	std::cout << "Remove booking" << std::endl;
	std::cout << "------------------------" << std::endl;
	get_all_free_seats();
	std::cout << "Select seat to remove" << std::endl;
	int chosen_seat;
	std::cin >> chosen_seat;
	if (chosen_seat > comedy_seats_available)
	{
		std::cout << "Wrong seat number, try again" << std::endl;
		add_booking();
	}
	else if (chosen_seat > 0)
	{
		all_booked[chosen_seat - 1].remove_booking();
	}
}