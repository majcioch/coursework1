#ifndef _FILM_H_
#define _FILM_H_
/*film.h
Author: M00770353<KU066@live.mdx.ac.uk>
Created: 13/01/2022
Updated: 14/01/2020
*/
#include "events.h"
#include "booking.h"
#include <iostream>

//class for film
class Film: public Events
{
	private: 
  static const int film_seats_available = 200;
	std::string film_type;
	int taken_seats;
	public: Booking all_booked[film_seats_available];
	Film();
	Film(std::string my_film_type, int taken_seats);~Film();
	void set_film_type(std::string my_film_type);
	std::string get_film_type();
	void add_booking();
	void remove_booking();
	int get_all_free_seats();
	void set_taken_seats(int my_taken_seats);
};

#endif