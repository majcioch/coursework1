#ifndef _COMEDY_H_
#define _COMEDY_H_
/* comedy.h
Author: M00770353 <KU066@live.mdx.ac.uk>
Created: 13/01/2022
Updated: 14/01/2020
*/
#include "events.h"
#include "booking.h"
#include <iostream>

//class for stand-up comedy
class Comedy: public Events {
   private: 
   static const int comedy_seats_available = 200;
   Booking all_booked[comedy_seats_available];
   int taken_seats;
   public: 
   Comedy();
   ~Comedy();
   void add_booking();
   void remove_booking();
   int get_all_free_seats();
   void set_taken_seats(int my_taken_seats);
};

#endif