/*film.cpp
Author: M00770353<KU066@live.mdx.ac.uk>
Created: 14/01/2022
Updated: 14/01/2020
*/

#include "film.h"

Film::Film()
{
	taken_seats = 0;
	for (int i = 0; i < film_seats_available; i++)
	{
		all_booked[i] = Booking("", "", i + 1, false);
	}
}

Film::Film(std::string my_film_type, int taken_seats)
{
	taken_seats = taken_seats;
	film_type = my_film_type;
	for (int i = 0; i < film_seats_available; i++)
	{
		all_booked[i] = Booking("", "", i + 1, false);
	}
}

Film::~Film() {}

void Film::set_taken_seats(int my_taken_seats)
//setup booking again if we read from file
{
	taken_seats = my_taken_seats;
	taken_seats = (film_seats_available - taken_seats);
	for (int i = 0; i < taken_seats; i++)
	{
		all_booked[i].add_booking_not_allocated();
      //adding booking for the value of taken seats that were in file
	}
}

int Film::get_all_free_seats()
//return how many free seats are left
{
	int free = 0;
	int i = 0;
	for (i = i; i < film_seats_available; i++)
	{
		if (!all_booked[i].is_reserved())
		{
			free++;
		}
	}

	return free;
}

void Film::add_booking()
//adding booking by passing first free seat to object of booking
{
	std::cout << "Adding booking" << std::endl;
	std::cout << "----------------------" << std::endl;
	int chosen_seat = 0;
	int i = 0;
	if (get_all_free_seats() != 0)
	{
		while (chosen_seat == 0)
		{
			if (!all_booked[i].is_reserved())
			{
				chosen_seat = i;
				all_booked[chosen_seat].add_booking_not_allocated();
				std::cout << "Seating ticket for : " <<
					get_event_name() << std::endl;
				chosen_seat++;	//for loop to break

			}

			i++;
		};
	}
	else
	{
		std::cout << "No free seats avible." << std::endl;
	}
}

void Film::remove_booking()
//removes highest index of booking
{
	std::cout << "Remove booking" << std::endl;
	std::cout << "-----------------------" << std::endl;
	int temp = 0;
	temp = 200 - get_all_free_seats();	//get number of booked seats
	all_booked[temp - 1].remove_booking();	//remove booking with index of temp-1(becouse counting from 0)

}

void Film::set_film_type(std::string my_film_type)
{
	film_type = my_film_type;
}

std::string Film::get_film_type()
{
	return film_type;

}