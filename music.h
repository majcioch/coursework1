#ifndef _MUSIC_H_
#define _MUSIC_H_
/*music.h
Author: M00770353<KU066@live.mdx.ac.uk>
Created: 13/01/2022
Updated: 14/01/2020
*/
#include "events.h"
#include "booking.h"
#include <iostream>

//class for live music
class Music: public Events
{
	private: 
        static const int music_seats_available = 300;
	Booking all_booked[music_seats_available];
	int taken_seats;
	public:
        Music();
	Music(int my_taken_seats);
        ~Music();
	void add_booking();
	void remove_booking();
	int get_all_free_seats();
	void set_taken_seats(int my_taken_seats);
};

#endif