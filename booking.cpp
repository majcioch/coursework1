/*booking.cpp
Author: M00770353<KU066@live.mdx.ac.uk>
Created: 13/01/2022
Updated: 14/01/2020
*/
#include "booking.h"
#include <iomanip>

Booking::Booking()
{
	id = 0;
}
Booking::~Booking() {}

Booking::Booking(std::string t_name, std::string t_surname,
	int t_id, bool t_booked)
{
	name = t_name;
	surname = t_surname;
	id = t_id;
	booked = t_booked;
}

bool Booking::is_reserved()	//returning true or false if object is booked
{
	return booked;
}

void Booking::print()
{
	//printing every seat (with name and surname if taken)
	if (booked)
	{
		std::cout << "Id: " << id << ", " << name << " " <<
			surname;
	}
	else
	{
		std::cout << "Seat id:" << std::setw(3) << id << "-Free " << "|| ";
	}
}

void Booking::add_booking()	//basicly changing bool booked to true and adding name and sruname
{
	if (booked)
	{
		std::cout << id << "- is allready booked." << std::endl;
	}
	else
	{
		std::cout << "Name: ";
		std::cin >> name;

		std::cout << "Surname: ";
		std::cin >> surname;

		booked = true;

		std::cout << "booking of:  " << id <<
			" has been added for: " << name <<
			" " << surname << "." << std::endl;
	}
}

void Booking::add_booking_not_allocated()
{
	//basicly changing bool booked to true but with no name
	booked = true;

	std::cout << "Booking Id - " << id <<
		" has been added," << std::endl;

}

void Booking::remove_booking()
{
	//chagning variables to initial state
	if (booked)
	{
		name = "";
		surname = "";
		booked = false;

		std::cout << "Removed booking successfully." << std::endl;
	}
	else
	{
		std::cout << "Wrong booking id" << std::endl;
	}
}