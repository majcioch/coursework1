#include "events.h"
/* events.cpp
Author: M00770353 <KU066@live.mdx.ac.uk>
Created: 06/01/2022
Updated: 14/01/2020
*/
Events::Events(){
}
Events::Events(std::string t_event_name,long t_event_id,std::string t_date,
	       double t_ticket_price)
{
  event_name = t_event_name;
  event_id = t_event_id;
  event_date = t_date;
  ticket_price = t_ticket_price;
}
Events::~Events(){
}
std::string Events::get_event_name(){
  return event_name;
}
std::string Events::get_event_date(){
  return event_date;
}
long Events::get_event_id(){
  return event_id;
}
void Events::set_event_date(std::string my_event_date){
   event_date= my_event_date;
}
void Events::set_event_id(long my_event_id){
   event_id= my_event_id;
}
void Events::set_ticket_price(double my_ticket_price){
  ticket_price=my_ticket_price;
}
void Events::set_event_name(std::string my_event_name){
  event_name=my_event_name;
}
double Events::get_ticket_price(){
  return ticket_price;
}
void Events::display_all(){
  std::cout<< "-------------------------------"<<std::endl;
  std::cout << "Id : " << get_event_id() << std::endl;
  std::cout << "Name : " << get_event_name() << std::endl;
  std::cout << "Date : " << get_event_date() << std::endl;
  std::cout << "Price : " << get_ticket_price()<<std::endl;
}
