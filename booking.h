#ifndef _BOOKING_H_
#define _BOOKING_H_
/* booking.h
Author: M00770353 <KU066@live.mdx.ac.uk>
Created: 13/01/2022
Updated: 14/01/2020
*/
#include <iostream>
//booking event
class Booking {
  public:
  std::string name;
  std::string surname;
  int id;
  bool booked;
  Booking();
  Booking(std::string my_name, std::string my_surname,
    int my_id, bool my_booked);
  ~Booking();
  void add_booking();
  void add_booking_not_allocated();
  void remove_booking();
  bool is_reserved();
  void print();
};
#endif
