#ifndef _EVENTS_H_
#define _EVENTS_H_
/* events.h
Author: M00770353 <KU066@live.mdx.ac.uk>
Created: 06/01/2022
Updated: 14/01/2020
*/
#include <iostream>

//main event class
class Events{
private:
  long event_id;
  double ticket_price;
  std::string event_date;
  std::string event_name;
public:

  Events();
  Events(std::string my_event_name,long t_event_id,std::string t_date,
	 double my_ticket_price);
  ~Events();
  double get_ticket_price();
  std::string get_event_name();
  std::string get_event_date();
  long get_event_id();
  void set_event_date(std::string my_event_date);
  void set_event_id(long my_event_id);
  void set_ticket_price(double my_ticket_price);
  void set_event_name(std::string my_event_name);
  void display_all();

};
#endif
