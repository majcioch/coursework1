#define CATCH_CONFIG_MAIN
/* TEST.cpp
Author: M00770353 <KU066@live.mdx.ac.uk>
Created: 13/01/2022
Updated: 14/01/2020
*/
#include <iostream> 
#include "catch.hpp"
#include "booking.h"
#include "comedy.h"
#include "events.h"
#include "film.h"
#include "music.h"

TEST_CASE("Checking booking system", "[get_all_free_seats]"){
  Film lotr("2d",0);

lotr.set_event_id(1);

REQUIRE(lotr.get_all_free_seats()==200);
//inital number of seats is 200

lotr.add_booking();
REQUIRE(lotr.get_all_free_seats()==199);
//after booking there should be 199 free seats left

lotr.remove_booking();
REQUIRE(lotr.get_all_free_seats()==200);
//after removing booking there should be 200 free seats left

lotr.set_taken_seats(150);
REQUIRE(lotr.get_all_free_seats()==150);
//after setting how many seats are free there should be booking of 50 seats and object should be left with 150 free seats

lotr.remove_booking();
lotr.remove_booking();
REQUIRE(lotr.get_all_free_seats()==152);
//after removing 2 bookings there should be 152 free seats left

}
